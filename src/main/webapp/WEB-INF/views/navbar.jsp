<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 30.07.2018
  Time: 11:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

    </head>

    <body>

        <!-- Static navbar -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container-fluid">

                <div class="navbar-header">
                    <%--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--%>
                        <%--<span class="sr-only">Toggle navigation</span>--%>
                        <%--<span class="icon-bar"></span>--%>
                        <%--<span class="icon-bar"></span>--%>
                        <%--<span class="icon-bar"></span>--%>
                    <%--</button>--%>

                    <a class="navbar-brand" href="<spring:url value="/" />">Home</a>
                    <a class="navbar-brand" href="<spring:url value="/allContacts" />">All Contacts</a>
                    <a class="navbar-brand" href="<spring:url value="/addContactForm" />">Add Contact</a>
                    <%--<a class="navbar-brand" href="<spring:url value="/addUserForm" />">Add User</a>--%>

                    <%--###############################################################--%>
                    <%--<a class="navbar-brand" href="<spring:url value="/" />">Home</a>--%>
                    <%--<a class="navbar-brand" href="<spring:url value="/allUsers" />">Admin</a>--%>
                    <%--<a class="navbar-brand" href="<spring:url value="/addUserForm" />">Add User</a>--%>
                    <%--###############################################################--%>
                </div>

                <%--<div class="navbar-collapse collapse">  nav navbar-nav--%>
                    <div class="nav nav navbar-nav mynavright">


                        <li><a href="<spring:url value="/allUsers"/>">Admin</a></li>
                        <li class="${current == 'index' ? 'active' : ''}"><a href="<spring:url value="/login" />">Login</a></li>
                        <li><a>You are logged in as ${loggedUser}</a></li>
                        <li><a href="<spring:url value="/logout" />">Logout</a></li>


                        <%--###############################################################--%>
                        <%--<li class="${current == 'login' ? 'active' : ''}"><a href="<spring:url value="/allContacts" />">All Contacts</a></li>--%>
                        <%--<li class="${current == 'register' ? 'active' : ''}"><a href="<spring:url value="/addContactForm" />">Add Contact</a></li>--%>
                        <%--<li class="${current == 'index' ? 'active' : ''}"><a href="<spring:url value="/login" />">Login</a></li>--%>
                        <%--<li><a href="<spring:url value="/logout" />">Logout</a></li>--%>
                        <%--<li><a>You are logged in as ${loggedUser}</a></li>--%>
                        <%--###############################################################--%>


                        <%--<security:authorize access="hasRole('ROLE_ADMIN')">--%>
                        <%--<li class="${current == 'myUser' ? 'active' : ''}"><a href="<spring:url value="/index.jsp" />">Users</a></li>--%>
                        <%--</security:authorize>--%>

                        <%--<security:authorize access="isAuthenticated()">--%>
                        <%--<li class="${current == 'account' ? 'active' : ''}"><a href="<spring:url value="/account.html" />">My account</a></li>--%>

                        <%--</security:authorize>--%>
                    </div>
                <%--</div><!--/.nav-collapse -->--%>
            </div><!--/.container-fluid -->
        </div>
    </body>
</html>
