<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="java.util.List" %>
<%@ page import="com.grzegorz.chudyk.entity.MyContact" %>

<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 29.07.2018
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">


    <script type="text/javascript" src="http://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


    <title>All Contact</title>

</head>

<body>

<div class="container">

    <%--###########################################################--%>

    <jsp:include page="../views/navbar.jsp" />

    <%--###########################################################--%>



    <h2>Wszystkie Kontakty</h2>
    <hr id="line" />

    <br>

    <table id="resultTable1" class="table table-condensed table-hover">
    <thead>
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Phone</th>

            <th>Usun</th>
            <th>Edytuj</th>
        </tr>
    </thead>

    <tbody>

        <%
            List<MyContact> allMyContactList = (List<MyContact>) request.getAttribute("allMyContactList");
            for (MyContact myContact : allMyContactList) {

        %>


        <tr>

            <td><%= myContact.getName() %></td>
            <td><%= myContact.getSurname()%></td>
            <td><%= myContact.getPhoneString()%></td>

            <td>
                <form action="deleteContact" method="post">
                    <input type="hidden" value=<%= myContact.getContactId()%> name="idContact" />
                    <input type="submit" value="Remove" />
                </form>
            </td>
            <td>
                <form action="editContactForm" method="post">
                    <input type="hidden" value=<%= myContact.getContactId()%> name="idContact" />
                    <input type="submit" value="Edytuj" />
                </form>
            </td>

        </tr>

        <%
            }
        %>
    </tbody>

    </table><br>

        <script type="text/javascript">
            $(document).ready(function() { $('#resultTable1').dataTable(); });
        </script>




    <%--###########################################################--%>

    <jsp:include page="../views/credits.jsp" />

</div>

</body>
</html>
