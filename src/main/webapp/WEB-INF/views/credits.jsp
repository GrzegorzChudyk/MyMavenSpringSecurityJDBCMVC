<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 29.07.2018
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

        <title>Credits</title>
    </head>

    <body>

    <%--###########################################################--%>

        <br>
        <br>
        <hr id="line" /><br>

        <div id="centerDiv">
            &copy; Grzegorz Chudyk
        </div>

    </body>
</html>
