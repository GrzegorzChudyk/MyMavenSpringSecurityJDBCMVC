<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 28.07.2018
  Time: 22:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

    <title>Home</title>

</head>

<body>

<%--<tilesx:useAttribute name="current"/>--%>

<div class="container">

    <%--###########################################################--%>

    <jsp:include page="../views/navbar.jsp" />

    <%--###########################################################--%>

        <%--<h3>Login with Username and Password</h3>--%>

        <%--<form class="form-signin" role="form" name="f" action="login" method="POST">--%>

            <%--<table>--%>
                <%--&lt;%&ndash;<span>${param.error}</span>&ndash;%&gt;--%>
                <%--<tr><td>Username:</td><td><input type="text" class="form-control" name="username" placeholder="Username" value=""></td></tr>--%>
                <%--<tr><td>Password:</td><td><input type="password" class="form-control" name="password" placeholder="Password"/></td></tr>--%>
                <%--&lt;%&ndash;<span>${msg}</span>&ndash;%&gt;--%>
                <%--<tr><td colspan="2"><input name="submit" type="submit" value="Login"/></td></tr>--%>
            <%--</table>--%>
        <%--</form>--%>

        <div id="loginForm">

            <form class="form-signin" role="form" action="/login" method="POST">
                <h2 class="form-signin-heading">Please sign in</h2>
                <input type="text" name="username" class="form-control" placeholder="Name" required autofocus="true"><br>
                <input type="password" name="password" class="form-control" placeholder="Password" required><br>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>

            <form class="form-signin" role="form" action="/addUserForm" method="POST">
                <button class="btn btn-lg btn-block mybutton" type="submit">Create New Account</button>
                <%--<button class="btn btn-lg btn-primary btn-block" type="submit">New User</button>--%>
            </form>

            <div id="badCredentials">
                <span>${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</span>
            </div>

        </div>

        <%--<form class="form-signin" role="form" action="/login" />" method="POST">--%>
            <%--<h2 class="form-signin-heading">Please sign in</h2>--%>
            <%--<input type="text" name="j_username" class="form-control" placeholder="Name" required autofocus>--%>
            <%--<input type="password" name="j_password" class="form-control" placeholder="Password" required>--%>
            <%--<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>--%>
        <%--</form>--%>

    <%--<tiles:insertAttribute name="body" />--%>


    <center>
        <%--<tiles:insertAttribute name="footer" />--%>
    </center>



    <%--###########################################################--%>

    <jsp:include page="../views/credits.jsp" />

</div>

</body>
</html>
