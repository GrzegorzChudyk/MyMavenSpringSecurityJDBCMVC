<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 29.07.2018
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

    <title>Edit Role</title>

</head>

<body>

<div class="container">

    <%--###########################################################--%>

    <jsp:include page="../views/navbar.jsp" />

    <%--###########################################################--%>

        <%
            String mycheck = "true";
        %>


    <h2>Edycja ról użytkownika: ${changedUserName}</h2>
    <hr id="line" />

    <br>
        <form class="mycell" action="saveRole" method="post">

            <input type="hidden" name="changedUserName" value="${changedUserName}">

            <table id="resultTable" class="table-condensed table-hover">

                <tr>
                    <th>ADMIN</th>
                    <%--<td><input type="checkbox" name="adminRole" value="ROLE_ADMIN" ${rolesTable[0] ? "checked" : ""} ></td>--%>
                    <td><input type="checkbox" name="role" value="ROLE_ADMIN" ${rolesTable[0] ? "checked" : ""} ></td>
                    <%--<%= ("true".equals(mycheck) ? "checked" : "") %>--%>
                </tr>

                <tr>
                    <th>USER</th>
                    <%--<td><input type="checkbox" name="userRole" value="ROLE_USER" ${rolesTable[1] ? "checked" : ""} ></td>--%>
                    <td><input type="checkbox" name="role" value="ROLE_USER" ${rolesTable[1] ? "checked" : ""} ></td>
                </tr>

                <tr>
                    <td colspan="2"><input type="submit" value="Save" /></td>
                </tr>
                <%--<tr>--%>
                    <%--<th>Name</th>--%>
                    <%--<th>Surname</th>--%>
                    <%--<th>Phone</th>--%>

                    <%--<th>Usun</th>--%>
                    <%--<th>Edytuj</th>--%>
                <%--</tr>--%>


                <%--<%--%>
                    <%--List<MyContact> allMyContactList = (List<MyContact>) request.getAttribute("allMyContactList");--%>
                    <%--for (MyContact myContact : allMyContactList) {--%>

                <%--%>--%>

                <%--<tr>--%>

                    <%--<td><%= myContact.getName() %></td>--%>
                    <%--<td><%= myContact.getSurname()%></td>--%>
                    <%--<td><%= myContact.getPhoneString()%></td>--%>

                    <%--<td>--%>
                        <%--<form action="deleteContact" method="post">--%>
                            <%--<input type="hidden" value=<%= myContact.getContactId()%> name="idContact" />--%>
                            <%--<input type="submit" value="Remove" />--%>
                        <%--</form>--%>
                    <%--</td>--%>
                    <%--<td>--%>
                        <%--<form action="editContactForm" method="post">--%>
                            <%--<input type="hidden" value=<%= myContact.getContactId()%> name="idContact" />--%>
                            <%--<input type="submit" value="Edytuj" />--%>
                        <%--</form>--%>
                    <%--</td>--%>

                <%--</tr>--%>

                <%--<%--%>
                    <%--}--%>
                <%--%>--%>

            </table><br>


        </form>


    <%--###########################################################--%>

    <jsp:include page="../views/credits.jsp" />

</div>

</body>
</html>
