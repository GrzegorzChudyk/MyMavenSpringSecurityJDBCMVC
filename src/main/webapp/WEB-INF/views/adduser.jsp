<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 27.07.2018
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

        <title>Add User</title>
    </head>

    <body>

        <div class="container">

            <!-- Static navbar -->
            <%--<div class="navbar navbar-default" role="navigation">--%>
                <%--<div class="container-fluid">--%>

                    <%--<div class="navbar-header">--%>
                        <%--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--%>
                            <%--<span class="sr-only">Toggle navigation</span>--%>
                            <%--<span class="icon-bar"></span>--%>
                            <%--<span class="icon-bar"></span>--%>
                            <%--<span class="icon-bar"></span>--%>
                        <%--</button>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/" />">JBA</a>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/allUsers" />">All MyUser</a>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/addUserForm" />">Add User</a>--%>
                    <%--</div>--%>

                    <%--<div class="navbar-collapse collapse">--%>
                        <%--<ul class="nav navbar-nav">--%>
                            <%--<li class="${current == 'index' ? 'active' : ''}"><a href="<spring:url value="/" />">Home</a></li>--%>

                            <%--&lt;%&ndash;<security:authorize access="hasRole('ROLE_ADMIN')">&ndash;%&gt;--%>
                            <%--<li class="${current == 'myUser' ? 'active' : ''}"><a href="<spring:url value="/index.jsp" />">MyUser</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>


                            <%--<li class="${current == 'register' ? 'active' : ''}"><a href="<spring:url value="/register.html" />">Register</a></li>--%>


                            <%--&lt;%&ndash;<security:authorize access="! isAuthenticated()">&ndash;%&gt;--%>
                            <%--<li class="${current == 'login' ? 'active' : ''}"><a href="<spring:url value="/login.html" />">Login</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>


                            <%--&lt;%&ndash;<security:authorize access="isAuthenticated()">&ndash;%&gt;--%>
                            <%--<li class="${current == 'account' ? 'active' : ''}"><a href="<spring:url value="/account.html" />">My account</a></li>--%>
                            <%--<li><a href="<spring:url value="/logout" />">Logout</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>
                        <%--</ul>--%>
                    <%--</div><!--/.nav-collapse -->--%>
                <%--</div><!--/.container-fluid -->--%>
            <%--</div>--%>

            <%--###########################################################--%>

            <jsp:include page="../views/navbar.jsp" />

            <%--###########################################################--%>
            <%--<h2 class="form-signin-heading">Create new User</h2>--%>

            <%--<li><h4>You are logged in as ${loggedUser == null ? 'gosc' : loggedUser}</h4></li>--%>

            <%--<hr>--%>

            <%--###########################################################--%>

            <%--<a href="/logout">Logout</a>--%>

            <div id="loginForm">

                <%--@elvariable id="myUser" type="com.grzegorz.chudyk.entity.MyUser"--%>
                <form:form class="form-signin" role="form" action="saveUser" modelAttribute="myUser" method="POST">

                    <h2 class="form-signin-heading">Create new User</h2>

                    <form:input type="text" class="form-control" placeholder="Username" path="username" autofocus="true"/>
                    <form:errors path="username" class="error"/>
                    <br>

                    <form:input type="text" class="form-control" placeholder="Password" path="password" />
                    <form:errors path="password" class="error"/>
                    <br>


                    <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button><br>

                    <button class="btn btn-lg btn-primary btn-block" type="reset">Reset</button>

                    <%--###########################################################--%>

                    <%--<table id="newContractTable">--%>

                        <%--<tr>--%>
                            <%--<td>Login</td>--%>
                            <%--<td><form:input type="text" placeholder="Username"  path="username"/></td>--%>
                            <%--<td><form:errors path="username" class="error"/></td>--%>
                        <%--</tr>--%>

                        <%--<tr>--%>
                            <%--<td>Password</td>--%>
                            <%--<td><form:input type="text" placeholder="Password" path="password" /></td>--%>
                            <%--<td><form:errors path="password" class="error"/></td>--%>
                        <%--</tr>--%>

                    <%--</table>--%>

                    <%--<br>--%>

                    <%--<div>--%>
                        <%--<input type="submit" name="action" value="Zapisz"/>--%>
                        <%--<input type="reset">--%>
                    <%--</div>--%>

                    <%--###########################################################--%>

                    <br>

                </form:form>

            </div>

            <%--###########################################################--%>

            <jsp:include page="../views/credits.jsp" />

        </div>

    </body>
</html>