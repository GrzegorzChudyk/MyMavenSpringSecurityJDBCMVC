<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="com.grzegorz.chudyk.entity.MyUser" %>
<%@ page import="java.util.List" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 29.07.2018
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

        <title>All Users</title>

    </head>

    <body>

        <div class="container">

            <%--<!-- Static navbar -->--%>
            <%--<div class="navbar navbar-default" role="navigation">--%>
                <%--<div class="container-fluid">--%>

                    <%--<div class="navbar-header">--%>
                        <%--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">--%>
                            <%--<span class="sr-only">Toggle navigation</span>--%>
                            <%--<span class="icon-bar"></span>--%>
                            <%--<span class="icon-bar"></span>--%>
                            <%--<span class="icon-bar"></span>--%>
                        <%--</button>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/" />">JBA</a>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/allUsers" />">All MyUser</a>--%>
                        <%--<a class="navbar-brand" href="<spring:url value="/addUserForm" />">Add User</a>--%>
                    <%--</div>--%>

                    <%--<div class="navbar-collapse collapse">--%>
                        <%--<ul class="nav navbar-nav">--%>
                            <%--<li class="${current == 'index' ? 'active' : ''}"><a href="<spring:url value="/" />">Home</a></li>--%>

                            <%--&lt;%&ndash;<security:authorize access="hasRole('ROLE_ADMIN')">&ndash;%&gt;--%>
                            <%--<li class="${current == 'myUser' ? 'active' : ''}"><a href="<spring:url value="/index.jsp" />">MyUser</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>


                            <%--<li class="${current == 'register' ? 'active' : ''}"><a href="<spring:url value="/register.html" />">Register</a></li>--%>


                            <%--&lt;%&ndash;<security:authorize access="! isAuthenticated()">&ndash;%&gt;--%>
                            <%--<li class="${current == 'login' ? 'active' : ''}"><a href="<spring:url value="/login.html" />">Login</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>


                            <%--&lt;%&ndash;<security:authorize access="isAuthenticated()">&ndash;%&gt;--%>
                            <%--<li class="${current == 'account' ? 'active' : ''}"><a href="<spring:url value="/account.html" />">My account</a></li>--%>
                            <%--<li><a href="<spring:url value="/logout" />">Logout</a></li>--%>
                            <%--&lt;%&ndash;</security:authorize>&ndash;%&gt;--%>
                        <%--</ul>--%>
                    <%--</div><!--/.nav-collapse -->--%>
                <%--</div><!--/.container-fluid -->--%>
            <%--</div>--%>

            <%--###########################################################--%>

            <jsp:include page="../views/navbar.jsp" />

            <%--###########################################################--%>



            <h2>Wszyscy Uzytkownicy</h2>
            <hr id="line" />

            <br>

            <table id="resultTable" class="table-condensed table-hover table-bordered">

                <tr>
                    <th>Login</th>
                    <%--<th>Haslo</th>--%>
                    <th colspan="2">Aktywnosc konta</th>
                    <%--<th>Acttivity</th>--%>
                    <th>Rola</th>
                    <th>Usun</th>

                </tr>


                <%
                    List<MyUser> allMyUserList = (List<MyUser>) request.getAttribute("allMyUserList");
                    for (MyUser myUser : allMyUserList) {
                %>

                <tr>

                    <td><%= myUser.getUsername() %></td>
                    <%--<td><%= myUser.getPassword()%></td>--%>
                    <td><%= myUser.isEnabled()%></td>

                    <td>
                        <form class="mycell" action="changeUserActivity" method="post">
                            <input type="hidden" value=<%= myUser.getUserId()%> name="idUser" />
                            <input type="submit" value="ON/OFF" />
                        </form>
                    </td>

                    <td>
                        <form class="mycell" action="editRoleForm" method="post">
                            <input type="hidden" value=<%= myUser.getUsername() %> name="changedUserName" />
                            <input type="submit" value="Edit" />
                        </form>
                    </td>

                    <td>
                        <form class="mycell" action="deleteUser" method="post">
                            <input type="hidden" value=<%= myUser.getUserId()%> name="idUser" />
                            <input type="submit" value="Remove" />
                        </form>
                    </td>


                </tr>

                <%
                    }
                %>

            </table>




            <%--###########################################################--%>

            <jsp:include page="../views/credits.jsp" />

        </div>

    </body>
</html>
