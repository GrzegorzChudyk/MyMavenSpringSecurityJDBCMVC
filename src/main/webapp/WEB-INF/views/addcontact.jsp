<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: grzes
  Date: 30.07.2018
  Time: 13:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/app.css" type="text/css">

        <title>Add Contact</title>
    </head>

    <body>

        <div class="container">

            <%--###########################################################--%>

            <jsp:include page="../views/navbar.jsp" />

            <%--###########################################################--%>
            <%--<h1>Spring Security - Hello World Example JDBC authorisation</h1>--%>
            <%--<h4>You are logged in as ${loggedUser}</h4>--%>

            <%--<hr>--%>
            <%--<hr>--%>
            <%--###########################################################--%>

            <div id="loginForm">

                <%--@elvariable id="myUser" type="com.grzegorz.chudyk.entity.MyContact"--%>
                <form:form class="form-signin" role="form" action="saveContact" modelAttribute="myContact" method="POST">

                    <h2 class="form-signin-heading">Add new Contact</h2>

                    <form:input type="text" class="form-control" placeholder="Name" path="name" autofocus="true"/>
                    <form:errors path="name" class="error"/>
                    <br>

                    <form:input type="text" class="form-control" placeholder="Surname" path="surname" />
                    <form:errors path="surname" class="error"/>
                    <br>

                    <form:input type="text" class="form-control" name="phone" placeholder="Phone Number" path="phone"/>
                    <form:errors path="phone" class="error">Brak numeru</form:errors>
                    <br>

                    <button class="btn btn-lg btn-primary btn-block" type="submit">Save</button><br>

                    <button class="btn btn-lg btn-primary btn-block" type="reset">Reset</button>


                    <%--<table id="newContractTable">--%>
    <%----%>
                        <%--<tr>--%>
                            <%--<td>Imie</td>--%>
                            <%--<td><form:input type="text" placeholder="Name"  path="name"/></td>--%>
                            <%--<td><form:errors path="name" class="error"/></td>--%>
                        <%--</tr>--%>
    <%----%>
                        <%--<tr>--%>
                            <%--<td>Nazwisko</td>--%>
                            <%--<td><form:input type="text" placeholder="Surname" path="surname" /></td>--%>
                            <%--<td><form:errors path="surname" class="error"/></td>--%>
                        <%--</tr>--%>
    <%----%>
                        <%--<tr>--%>
                            <%--<td >Numer telefonu</td>--%>
                            <%--<td><form:input type="text" name="phone" placeholder="Phone Number" path="phone"/></td>--%>
                            <%--<td><form:errors path="phone" class="error">Brak numeru</form:errors></td>--%>
                        <%--</tr>--%>
    <%----%>
                    <%--</table>--%>
    <%----%>
                    <%--<br>--%>
    <%----%>
                    <%--<div>--%>
                        <%--<input type="submit" name="action" value="Zapisz"/>--%>
                        <%--<input type="reset"/>--%>
                    <%--</div>--%>
    <%----%>

                </form:form>

            </div>


            <%--###########################################################--%>

            <jsp:include page="../views/credits.jsp" />

        </div>

    </body>
</html>
