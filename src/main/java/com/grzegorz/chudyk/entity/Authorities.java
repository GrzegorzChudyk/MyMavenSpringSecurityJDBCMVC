package com.grzegorz.chudyk.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Entity class of Authorities
 *
 * @author Grzegorz Chudyk
 */
@Entity
public class Authorities {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int authorityId;

    //@NotEmpty(message = "Name cannot be empty")
    //@Size(min = 2, max = 50, message = "Name must be more than 2 characters")
    private String username;

    //@NotEmpty(message = "Surname cannot be empty")
    //@Size(min = 2, max = 50, message = "Name must be more than 2 characters")
    private String authority;

    /**
     * Constructor
     */
    public Authorities() {
    }

    /**
     * Constructor
     *
     * @param username name of Username
     * @param authority authority of Username
     */
    public Authorities(String username, String authority) {
        this.username = username;
        this.authority = authority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "Authorities{" +
                "authorityId=" + authorityId +
                ", login='" + username + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}
