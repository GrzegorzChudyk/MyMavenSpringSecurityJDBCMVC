package com.grzegorz.chudyk.entity;

import com.grzegorz.chudyk.annotation.UniqueUsername;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

/**
 * Entity class of MyUser
 *
 * @author Grzegorz Chudyk
 */
@Entity
public class MyUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;


    @NotEmpty(message = "Username cannot be empty")
    @Size(min = 2, max = 50, message = "Name must be more than 2 characters")
    @UniqueUsername(message = "This user already exist")
    private String username;

    @NotEmpty(message = "Password cannot be empty")
    @Size(min = 2, max = 100, message = "Password must be more than 2 characters")
    private String password;

    private boolean enabled = true;

    @OneToMany(mappedBy="myUser", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    //@Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<MyContact> myContacts = new HashSet<MyContact>();

    /**
     * Constructor
     */
    public MyUser() {
    }

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;//.substring(0, 5);
    }

    public void setPassword(String password) {
        if(password.length() < 2)
            this.password = "";
        else
            this.password = new BCryptPasswordEncoder().encode(password);
    }

    public Set<MyContact> getMyContacts() {
        return myContacts;
    }

    public void setMyContacts(Set<MyContact> myContacts) {
        this.myContacts = myContacts;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }



    @Override
    public String toString() {
        return "MyUser{" +
                "userId=" + userId +
                ", login='" + username + '\'' +
                ", password='" + password + '\'' +
                ", active=" + enabled +
                '}';
    }
}
