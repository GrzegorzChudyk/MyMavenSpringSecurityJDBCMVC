package com.grzegorz.chudyk.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import static org.hibernate.annotations.CascadeType.SAVE_UPDATE;

/**
 * Entity class of MyContact
 *
 * @author Grzegorz Chudyk
 */
@Entity
public class MyContact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int contactId;

    @NotEmpty(message = "Name cannot be empty")
    @Size(min = 2, max = 20, message = "Name must be more than 2 characters")
    private String name;

    @NotEmpty(message = "Surname cannot be empty")
    @Size(min = 2, max = 20, message = "Name must be more than 2 digits")
    private String surname;

    @NotEmpty(message = "Phone number cannot be empty")
    @Size(min = 9, max = 9, message = "Phone number must contain 9 characters")
    private String phone;

    //name to nazwa kolumny jaka powstanie z kluczem obcym o nazwie fkidautor w articles
    @ManyToOne//(cascade = CascadeType.ALL)
    @Cascade(SAVE_UPDATE)
    @JoinColumn(name="fk_id_user")//, foreignKey=@ForeignKey(name="fkidautor"))
    private MyUser myUser;

    /**
     * Constructor
     */
    public MyContact() {
    }

    /**
     * Constructor
     *
     * @param name name of MyContact
     * @param surname surname of MyContact
     * @param phone phone number of MyContact
     */
    public MyContact(@NotEmpty(message = "Name cannot be empty") @Size(min = 2, max = 20, message = "Name must be more than 2 characters") String name, @NotEmpty(message = "Surname cannot be empty") @Size(min = 2, max = 20, message = "Name must be more than 2 characters") String surname, @NotEmpty(message = "Phone number cannot be empty") @Size(min = 9, max = 9, message = "Name must be more than 2 characters") String phone) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getContactId() {
        return contactId;
    }

    public String getPhone() {
        return phone;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public MyUser getMyUser() {
        return myUser;
    }

    public void setMyUser(MyUser myUser) {
        this.myUser = myUser;
    }

    @Override
    public String toString() {
        return "MyContact{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", phone=" + phone +
                '}';
    }

    /**
     * Custom Method to String for phone number
     *
     * @return formated phone number String
     */
    public String getPhoneString() {
        int myPhone = Integer.parseInt(this.phone);

        return String.format("%,d", myPhone);

        //return myPhone;
    }
}
