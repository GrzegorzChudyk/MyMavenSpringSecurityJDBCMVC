package com.grzegorz.chudyk.service;

import com.grzegorz.chudyk.service.dao.AuthoritiesDao;
import com.grzegorz.chudyk.service.dao.UsersDao;
import com.grzegorz.chudyk.entity.Authorities;
import com.grzegorz.chudyk.entity.MyUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * This service class provide method for user and implements userservice interface
 *
 * @author Grzegorz Chudyk
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    public UsersDao usersDao;

    @Autowired
    public AuthoritiesDao authoritiesDao;

    /**
     * Method that save user
     *
     * @param myUser myUser object to save
     * @param authorities Authorities object for saved user to set him authority
     */
    @Override
    public void saveUser(MyUser myUser, Authorities authorities) {
        usersDao.saveUser(myUser);
        authoritiesDao.saveAuthority(authorities);
    }

    /**
     * Method that allow to verify if the username exist in database
     *
     * @param username the name of user to verify in database
     * @return boolean value, true if username doesn't exist and false if exist
     */
    @Override
    public boolean uniqueVerification(String username) {
        return usersDao.uniqueVerification(username);
    }

    /**
     * Method that delete a MyUser object from database
     *
     * @param idUser id of MyUser that should be deleted
     */
    @Override
    public void deleteUser(int idUser) {

        //tutaj wazna kolejnosc ale moze lepiej zrobic zlaczenie tablic i fk w authorities tak samo jak w kontaktach cascade
        authoritiesDao.deleteAuthority(idUser);
        usersDao.deleteUser(idUser);
    }

    /**
     * Method that read all users
     *
     * @return all users list
     */
    @Override
    public List<MyUser> readAllUsers() {
        return usersDao.readAllUsers();
    }

    /**
     * Method that allow to change an user activity account status
     *
     * @param idUser user id whose account activity status should be changed
     */
    @Override
    public void changeUserActivity(int idUser) {
        usersDao.changeUserActivity(idUser);
    }
}
