package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.MyContact;
import com.grzegorz.chudyk.entity.MyUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represent data access object of contacts and implements contactdao interface
 *
 * @author Grzegorz Chudyk
 */
@Repository
@Transactional
public class ContactDaoImpl implements ContactDao {

    @Autowired(required = true)
    private SessionFactory sessionFactory;

    /**
     * Method that save Contact object to database assigned to specific user by username
     *
     * @param myContact object of Contact to save in database
     * @param myUser the name of user to bind in with Contact
     */
    public void saveContact(MyContact myContact, MyUser myUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //add contact to logged user
        myContact.setMyUser(myUser);

        //save contact
        session.save(myContact);
    }

    /**
     * Method that delete Contact object by Contact id
     *
     * @param id of contact that should be deleted
     */
    public void deleteContact(int id) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //finding contact by id
        MyContact myContact = session.find(MyContact.class, id);

        //deleting founded contact
        session.remove(myContact);
    }

    /**
     * Method that read all contact assigned to logged user
     *
     * @param loggedUser the name of user which contact we read
     * @return contact list of logged user
     */
    public List<MyContact> readAllContacts(String loggedUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query myUser in HQL by username
        String hql = new String("FROM MyUser M WHERE M.username=:username");
        MyUser myUser = (MyUser) session.createQuery(hql).setParameter("username", loggedUser).getSingleResult();

        //getting list of contacts for logged user
        //it is enough for fetch type EAGER IN MyUser @OneToMany myContacts probably everywhere in this project
        List<MyContact> contactsList = new ArrayList<MyContact>(myUser.getMyContacts());

        return contactsList;
    }
}
