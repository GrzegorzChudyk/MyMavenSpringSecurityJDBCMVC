package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.MyUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * This class represent data access object of users and implements usersdao interface
 *
 * @author Grzegorz Chudyk
 */
@Repository
@Transactional
public class UsersDaoImpl implements UsersDao {

    @Autowired(required = true)
    private SessionFactory sessionFactory;

    /**
     * Method that save MyUser object to database
     *
     * @param myUser MyUser object to save
     */
    public void saveUser(MyUser myUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //saving myUser
        session.save(myUser);
    }

    /**
     * Method that allow to verify if the username exist in database
     *
     * @param username the name of user to verify in database
     * @return boolean value, true if username doesn't exist and false if exist
     */
    public boolean uniqueVerification(String username) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query myUser in HQL by username
        String hql = new String("FROM MyUser M WHERE M.username=:username");
        List<MyUser> myUsersList = session.createQuery(hql).setParameter("username", username).getResultList();

//        if(myUsersList.isEmpty()) {
//            System.out.println("SOM MY W null");
//            return true;
//        } else {
//            System.out.println("nie SOM MY W null");
//            return false;
//        }

        //this "return" instead above if block
        return (myUsersList.isEmpty() ? true : false);
    }

    /**
     * Method that delete a MyUser object from database
     *
     * @param idUser id of MyUser that should be deleted
     */
    public void deleteUser(int idUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        MyUser myUser = session.find(MyUser.class, idUser);
        session.remove(myUser);
    }

    /**
     * Method that return a MyUser object found in database by loggedUsername
     *
     * @param loggedUsername the name of logged user name
     * @return an MyUser object
     */
    public MyUser getLoggedUserByName(String loggedUsername) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query myUser in HQL by username
        String hql = new String("FROM MyUser M WHERE M.username=:username");
        MyUser myUser = (MyUser) session.createQuery(hql).setParameter("username", loggedUsername).getSingleResult();

        return myUser;
    }

    /**
     * Method that read all users
     *
     * @return all users list
     */
    public List<MyUser> readAllUsers() {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query all users list in HQL
        String hql = new String("from MyUser");
        List<MyUser> myUserList = session.createQuery(hql).getResultList();

        return myUserList;
    }

    /**
     * Method that allow to change an user activity account status
     *
     * @param idUser user id whose account activity status should be changed
     */
    @Override
    public void changeUserActivity(int idUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        MyUser myUser = session.find(MyUser.class, idUser);
        myUser.setEnabled(!myUser.isEnabled());
        session.save(myUser);
    }
}
