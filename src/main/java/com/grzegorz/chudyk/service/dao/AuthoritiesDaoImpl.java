package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.Authorities;
import com.grzegorz.chudyk.entity.MyUser;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * This class represent data access object of authorities and implements authoritiesdao interface
 *
 * @author Grzegorz Chudyk
 */
@Repository
@Transactional
public class AuthoritiesDaoImpl implements AuthoritiesDao {

    @Autowired(required = true)
    private SessionFactory sessionFactory;

    /**
     * Method that save Authority object to database
     *
     * @param authorities Authorities object to save
     */
    public void saveAuthority(Authorities authorities) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //save authoritiy
        session.save(authorities);
    }

    /**
     * Method that delete Authority object by user id
     *
     * @param id of uset that should be deleted
     */
    @Override
    public void deleteAuthority(int id) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //separate this two query into AuthoritiesDaoImp and UsersDaoImp would be better
        // but now there is one session opened to delete authorities and DB is less busy
        //hql query finding user by id
        String hql = new String("FROM MyUser WHERE userId=:userid");
        MyUser myUser = (MyUser) session.createQuery(hql).setParameter("userid", id).getSingleResult();

        //hql query deleting authorities for unique user by his username
        String hql2 = new String("DELETE FROM Authorities A WHERE A.username=:username");
        session.createQuery(hql2).setParameter("username", myUser.getUsername()).executeUpdate();
    }

    /**
     * Method that delete specific Authority by username and name of authority
     *
     * @param role name of authority to delete
     * @param changedUser the name of user whose authority should be deleted
     */
    @Override
    public void deleteAuthority(String role, String changedUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        String hql = new String("DELETE FROM Authorities A WHERE A.username=:changedUser AND A.authority=:role");
        session.createQuery(hql).setParameter("changedUser", changedUser).setParameter("role", role).executeUpdate();
    }

    /**
     * Method that checks whether an authority exist by role and username
     *
     * @param role name of authority to check if exist
     * @param changedUser the name of user to check if his authority exist
     * @return boolean value true if exist, false if doesn't
     */
    @Override
    public boolean existAuthority(String role, String changedUser) {

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query myUser in HQL by username
        String hql = new String("FROM Authorities A WHERE A.username=:changedUser AND A.authority=:role");
        List<Authorities> myAuthoritiesList = session.createQuery(hql).setParameter("changedUser", changedUser).setParameter("role", role).getResultList();


        //this "return" instead above if block
        return (myAuthoritiesList.isEmpty() ? false : true);
    }


    public List<Authorities> readAllAuthorities() {
        return null;
    }

    /**
     * Method that indicate roles of specific user.
     *
     * @param userName the name of user to check his authorities
     * @return boolean table on index 0 if has admin role, on index 1 if has user role
     */
    @Override
    public boolean[] checkUserAuthorities(String userName) {

        boolean authorities[] = {false, false};

        //getting session
        Session session = sessionFactory.getCurrentSession();

        //query myUser in HQL by username
        String hql = new String("FROM Authorities A WHERE A.username=:changedUser");
        List<Authorities> myAuthoritiesList = session.createQuery(hql).setParameter("changedUser", userName).getResultList();

        if(!myAuthoritiesList.isEmpty()) {
            for (int i = 0; i < myAuthoritiesList.size(); i++) {
                if("ROLE_ADMIN".equals(myAuthoritiesList.get(i).getAuthority())) {
                    authorities[0] = true;
                }

                if("ROLE_USER".equals(myAuthoritiesList.get(i).getAuthority())) {
                    authorities[1] = true;
                }
            }
        }

        return authorities;
    }
}
