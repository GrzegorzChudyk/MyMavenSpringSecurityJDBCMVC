package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.MyContact;
import com.grzegorz.chudyk.entity.MyUser;
import org.hibernate.Session;

import java.util.List;

/**
 * This interface provides methods for Contacts.
 *
 * @author Grzegorz Chudyk
 */
public interface ContactDao {

    /**
     * Method that save Contact object to database assigned to specific user by username
     *
     * @param myContact object of Contact to save in database
     * @param myUser the name of user to bind in with Contact
     */
    public void saveContact(MyContact myContact, MyUser myUser);

    /**
     * Method that delete Contact object by Contact id
     *
     * @param id of contact that should be deleted
     */
    public void deleteContact(int id);

    /**
     * Method that read all contact assigned to logged user
     *
     * @param loggedUser the name of user which contact we read
     * @return contact list of logged user
     */
    public List<MyContact> readAllContacts(String loggedUser);
}
