package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.MyUser;

import java.util.List;

/**
 * This interface provides methods for users.
 *
 * @author Grzegorz Chudyk
 */
public interface UsersDao {

    /**
     * Method that save MyUser object to database
     *
     * @param myUser MyUser object to save
     */
    void saveUser(MyUser myUser);

    /**
     * Method that allow to verify if the username exist in database
     *
     * @param username the name of user to verify in database
     * @return boolean value, true if username doesn't exist and false if exist
     */
    boolean uniqueVerification(String username);

    /**
     * Method that delete a MyUser object from database
     *
     * @param idUser id of MyUser that should be deleted
     */
    void deleteUser(int idUser);

    /**
     * Method that return a MyUser object found in database by loggedUsername
     *
     * @param loggedUsername the name of logged user name
     * @return an MyUser object
     */
    MyUser getLoggedUserByName(String loggedUsername);

    /**
     * Method that read all users
     *
     * @return all users list
     */
    List<MyUser> readAllUsers();

    /**
     * Method that allow to change an user activity account status
     *
     * @param idUser user id whose account activity status should be changed
     */
    void changeUserActivity(int idUser);
}
