package com.grzegorz.chudyk.service.dao;

import com.grzegorz.chudyk.entity.Authorities;

import java.util.List;

/**
 * This interface provides methods for Authorities.
 *
 * @author Grzegorz Chudyk
 */
public interface AuthoritiesDao {

    /**
     * Method that save Authority object to database
     *
     * @param authorities Authorities object to save
     */
    public void saveAuthority(Authorities authorities);

    /**
     * Method that delete Authority object by user id
     *
     * @param id of user that should be deleted
     */
    public void deleteAuthority(int id);

    /**
     * Method that delete specific Authority by username and name of authority
     *
     * @param role name of authority to delete
     * @param changedUser the name of user whose authority should be deleted
     */
    public void deleteAuthority(String role, String changedUser);

    /**
     * Method that checks whether an authority exist by role and username
     *
     * @param role name of authority to check if exist
     * @param changedUser the name of user to check if his authority exist
     * @return boolean value true if exist, false if doesn't
     */
    public boolean existAuthority(String role, String changedUser);

    /**
     *
     * @return
     */
    public List<Authorities> readAllAuthorities();

    /**
     * Method that indicate roles of specific user.
     *
     * @param userName the name of user to check his authorities
     * @return boolean table on index 0 if has admin role, on index 1 if has user role
     */
    public boolean[] checkUserAuthorities(String userName);
}
