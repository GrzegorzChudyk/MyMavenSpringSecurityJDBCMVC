package com.grzegorz.chudyk.service;

/**
 * This service interface provides methods for Authorities.
 *
 * @author Grzegorz Chudyk
 */
public interface AuthoritiesService {

    /**
     * Method that add or remove Authorities for specific user and save in db
     *
     * @param adminRole indicate if user have an admin role
     * @param userRole indicate if user have an user role
     * @param changedUser indicate name of user to edit authorities
     */
    void editRoles(String adminRole, String userRole, String changedUser);

    /**
     * Method that add or remove Authorities for specific user and save in db
     *
     * @param roles roles table of user, table on index 0 admin role, on index 1 user role
     * @param changedUser indicate name of user to edit authorities
     */
    void editRoles(String roles[], String changedUser);

    /**
     * Method that allow check what roles have an user
     *
     * @param changedUserName name of user to check his roles
     * @return table of roles, table on index 0 if has admin role, on index 1 if has user role
     */
    boolean[] checkUserAuthorities(String changedUserName);
}
