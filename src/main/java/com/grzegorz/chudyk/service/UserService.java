package com.grzegorz.chudyk.service;

import com.grzegorz.chudyk.entity.Authorities;
import com.grzegorz.chudyk.entity.MyUser;

import java.util.List;

/**
 * This service interface provides methods for User.
 *
 * @author Grzegorz Chudyk
 */
public interface UserService {

    /**
     * Method that save user
     *
     * @param myUser myUser object to save
     * @param authorities Authorities object for saved user to set him authority
     */
    void saveUser(MyUser myUser, Authorities authorities);

    /**
     * Method that allow to verify if the username exist in database
     *
     * @param username the name of user to verify in database
     * @return boolean value, true if username doesn't exist and false if exist
     */
    boolean uniqueVerification(String username);

    /**
     * Method that delete a MyUser object from database
     *
     * @param idUser id of MyUser that should be deleted
     */
    void deleteUser(int idUser);

    /**
     * Method that read all users
     *
     * @return all users list
     */
    List<MyUser> readAllUsers();

    /**
     * Method that allow to change an user activity account status
     *
     * @param idUser user id whose account activity status should be changed
     */
    void changeUserActivity(int idUser);
}
