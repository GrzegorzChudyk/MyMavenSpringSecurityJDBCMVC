package com.grzegorz.chudyk.service;

import com.grzegorz.chudyk.entity.MyContact;

import java.util.List;

/**
 * This service interface provides methods for Contact.
 *
 * @author Grzegorz Chudyk
 */
public interface ContactService {

    /**
     * Method that save the contact
     *
     * @param myContact MyContact object to save
     * @param loggedUser name of logged in user who saves the contact
     */
    public void saveContact(MyContact myContact, String loggedUser);

    /**
     * Method that delete contact by id
     *
     * @param id id of contact to delete
     */
    public void deleteContact(int id);

    /**
     * Method gets list of all contact of logged user
     *
     * @param loggedUser name of logged user
     * @return list of all contact of logged user
     */
    public List<MyContact> readAllContacts(String loggedUser);
}
