package com.grzegorz.chudyk.service;

import com.grzegorz.chudyk.service.dao.ContactDao;
import com.grzegorz.chudyk.service.dao.UsersDao;
import com.grzegorz.chudyk.entity.MyContact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This service class provide method for Contact and implements contactservice interface
 *
 * @author Grzegorz Chudyk
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    public ContactDao contactDao;

    @Autowired
    public UsersDao usersDao;

    /**
     * Method that save the contact
     *
     * @param myContact MyContact object to save
     * @param loggedUser name of logged in user who saves the contact
     */
    @Override
    public void saveContact(MyContact myContact, String loggedUser) {
        contactDao.saveContact(myContact, usersDao.getLoggedUserByName(loggedUser));
    }

    /**
     * Method that delete contact by id
     *
     * @param id id of contact to delete
     */
    @Override
    public void deleteContact(int id) {
        contactDao.deleteContact(id);
    }

    /**
     * Method gets list of all contact of logged user
     *
     * @param loggedUser name of logged user
     * @return list of all contact of logged user
     */
    @Override
    public List<MyContact> readAllContacts(String loggedUser) {
        return contactDao.readAllContacts(loggedUser);
    }
}
