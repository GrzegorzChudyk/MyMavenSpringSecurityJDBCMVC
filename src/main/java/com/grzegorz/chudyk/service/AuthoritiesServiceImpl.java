package com.grzegorz.chudyk.service;

import com.grzegorz.chudyk.service.dao.AuthoritiesDao;
import com.grzegorz.chudyk.entity.Authorities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This service class provide method for Authorities and implements authoritiesservice interface
 *
 * @author Grzegorz Chudyk
 */
@Service
public class AuthoritiesServiceImpl implements AuthoritiesService {

    @Autowired
    private AuthoritiesDao authoritiesDao;


    //this for each individual role
    /**
     * Method that add or remove Authorities for specific user and save in db
     *
     * @param adminRole indicate if user have an admin role
     * @param changedUser indicate if user have an user role
     * @param changedUser indicate name of user to edit authorities
     */
    @Override
    public void editRoles(String adminRole, String userRole, String changedUser) {

        if(adminRole == null) {
            authoritiesDao.deleteAuthority("ROLE_ADMIN", changedUser);
        } else {
            if(!authoritiesDao.existAuthority("ROLE_ADMIN", changedUser)) {

                Authorities authorities = new Authorities();
                authorities.setAuthority("ROLE_ADMIN");
                authorities.setUsername(changedUser);

                authoritiesDao.saveAuthority(authorities);
            }
        }

        if(userRole == null) {
            authoritiesDao.deleteAuthority("ROLE_USER", changedUser);
        } else {
            if(!authoritiesDao.existAuthority("ROLE_USER", changedUser)) {

                Authorities authorities = new Authorities();
                authorities.setAuthority("ROLE_USER");
                authorities.setUsername(changedUser);

                authoritiesDao.saveAuthority(authorities);
            }
        }

    }

    //this for role table
    /**
     * Method that add or remove Authorities for specific user and save in db
     *
     * @param roles roles table of user, table on index 0 admin role, on index 1 user role
     * @param changedUser indicate name of user to edit authorities
     */
    @Override
    public void editRoles(String[] roles, String changedUser) {

        boolean authoritiesTable[] = {false, false};

        if(roles != null) {
            for(int i = 0; i < roles.length; i++) {
                if("ROLE_ADMIN".equals(roles[i])) {
                    authoritiesTable[0] = true;
                }

                if("ROLE_USER".equals(roles[i])) {
                    authoritiesTable[1] = true;
                }
            }
        }

        if(authoritiesTable[0]) {
            if(!authoritiesDao.existAuthority("ROLE_ADMIN", changedUser)) {

                Authorities authorities = new Authorities();
                authorities.setAuthority("ROLE_ADMIN");
                authorities.setUsername(changedUser);

                authoritiesDao.saveAuthority(authorities);
            }
        } else {
            authoritiesDao.deleteAuthority("ROLE_ADMIN", changedUser);
        }

        if(authoritiesTable[1]) {
            if(!authoritiesDao.existAuthority("ROLE_USER", changedUser)) {
                Authorities authorities = new Authorities();
                authorities.setAuthority("ROLE_USER");
                authorities.setUsername(changedUser);

                authoritiesDao.saveAuthority(authorities);
            }
        } else {
            authoritiesDao.deleteAuthority("ROLE_USER", changedUser);
        }
    }

    /**
     * Method that allow check what roles have an user
     *
     * @param changedUserName name of user to check his roles
     * @return table of roles, table on index 0 if has admin role, on index 1 if has user role
     */
    @Override
    public boolean[] checkUserAuthorities(String changedUserName) {
        return authoritiesDao.checkUserAuthorities(changedUserName); // new boolean[0];
    }
}
