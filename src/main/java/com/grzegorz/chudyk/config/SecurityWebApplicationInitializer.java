package com.grzegorz.chudyk.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Security Web Application Initializer class
 *
 * @author Grzegorz Chudyk
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {
}
