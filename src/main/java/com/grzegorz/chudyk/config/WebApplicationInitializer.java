package com.grzegorz.chudyk.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Web Application Initializer class
 *
 * @author Grzegorz Chudyk
 */
public class WebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    /**
     * Specify @Configuration and/or @Component classes for the root application context.
     *
     * @author Grzegorz Chudyk
     */
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {WebSecurityConfig.class, HibernateConfiguration.class};
    }

    /**
     * Specify @Configuration and/or @Component classes for the Servlet application context.
     *
     * @author Grzegorz Chudyk
     */
    protected Class<?>[] getServletConfigClasses() {
        return new Class[] {WebConfig.class};
    }

    /**
     * Specify the servlet mapping(s) for the DispatcherServlet
     *
     * @author Grzegorz Chudyk
     */
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
