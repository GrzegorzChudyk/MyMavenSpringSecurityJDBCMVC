package com.grzegorz.chudyk.config;

import com.grzegorz.chudyk.utils.CustomAccessDeniedHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;
import java.util.Arrays;

/**
 * Web Security Configuration class
 *
 * @author Grzegorz Chudyk
 */
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    /**
     * Method configuring JDBC Authentication
     *
     * @param auth Authentication Manager Builder
     * @throws Exception
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username, password, enabled from myuser where username = ?")
                .authoritiesByUsernameQuery("select username, authority from authorities where username = ?")
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    /**
     * Method configuring Authorize Request
     *
     * @param http Http Security
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //.antMatchers("/").permitAll() //hasAnyRole("ADMIN")
                .antMatchers("/allUsers").hasAnyRole("ADMIN")
                .antMatchers("/editRoleForm").hasAnyRole("ADMIN")
                .antMatchers("/allContacts").hasAnyRole("ADMIN", "USER")
                .antMatchers("/addContactForm").hasAnyRole("ADMIN", "USER")
                .antMatchers("/login").permitAll()
                .antMatchers("/addUserForm").permitAll()
                .anyRequest().permitAll()

                .and()
                .exceptionHandling().accessDeniedPage("/accessDenied")//.accessDeniedHandler(accessDeniedHandler())

                .and()
                .formLogin().loginPage("/login")//.successForwardUrl("/addContactForm").failureForwardUrl("/login?error")
                .and()
                .logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/")
                .and()
                .csrf().disable();
    }

    //this bean is required for custom access denied page
    /**
     * Method return Bean Custom Access Denied Handler
     *
     * @return Custom Access Denied Handler
     */
    @Bean
    public AccessDeniedHandler accessDeniedHandler(){
        return new CustomAccessDeniedHandler();
    }

    //mozna wywalic
    public static void main(String[] args) {
        //to tutaj zeby sobie zakodowac haslo "admin123" i zakodowane wkleic wyzej
        System.out.println(new BCryptPasswordEncoder().encode("admin"));

        boolean authorities[] = new boolean[] {false, false};

        System.out.println(Arrays.toString(authorities));


//        if(!myUsersList.isEmpty()) {
//            for (int i = 0; i < 2; i++) {
//
//            }
//        }


    }
}
