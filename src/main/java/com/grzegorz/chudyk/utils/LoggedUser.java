package com.grzegorz.chudyk.utils;

import org.springframework.stereotype.Component;

import java.security.Principal;

/**
 * LoggedUser class
 *
 * @author Grzegorz Chudyk
 */
@Component
public class LoggedUser {

    /**
     * Constructor
     */
    public LoggedUser() {
    }

    /**
     * Method specify current logged user
     *
     * @param principal Principal object that indicate current logged user
     * @return current logged user in String
     */
    public String getLoggedUser(Principal principal) {

        if(principal == null) {
            return "Gosc";
        } else {
            return principal.getName();
        }
    }
}
