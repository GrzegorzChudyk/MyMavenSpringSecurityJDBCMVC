package com.grzegorz.chudyk.controller;

import com.grzegorz.chudyk.entity.MyContact;
import com.grzegorz.chudyk.service.ContactService;
import com.grzegorz.chudyk.utils.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Controller class for Contacts
 *
 * @author Grzegorz Chudyk
 */
@Controller
public class ContactController {

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    private ContactService contactService;

    /**
     * Method that supports request mapping to addContactForm
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to addcontact.jsp page
     */
    @RequestMapping(value = {"addContactForm"})
    public ModelAndView addContact(Principal principal){

        ModelAndView modelAndView = new ModelAndView("addcontact");
        modelAndView.addObject("myContact", new MyContact());

        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    //nie musi byc "saveContact(HttpServletRequest request)" bo FORM zwraca obiekt od razu @ModelAttribute("myContact")

    /**
     * Method that supports request mapping to saveContact
     *
     * @param myContact MyContact object received from addcontact.jsp page
     * @param result BindingResult object indicate if validation of object has errors
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to addcontact.jsp page or redirect to allcontact.jsp page
     */
    @RequestMapping(value = {"saveContact"}, method = RequestMethod.POST)
    public ModelAndView saveContact(@Valid @ModelAttribute("myContact") MyContact myContact, BindingResult result, Principal principal) {//, Model model, ) {

        //nie bindowalo z tagiem form poniewaz dla tagu form niezbedne bylo wyslanie obiektu np pustego
        //w tym celu w RequestMapping addUserForm nalezalo w modelu wyslac pusty obiekt contact!!
        if (result.hasErrors()) {

            ModelAndView modelAndView = new ModelAndView("addcontact");
            modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));
            //modelAndView.addObject(model);

            return modelAndView;
        } else {

            contactService.saveContact(myContact, principal.getName());

            //tutaj zwracamy redirect w modelu zeby nie powtarzac zapytania do db ktore juz jest w allContacts
            ModelAndView modelAndView = new ModelAndView("redirect:/allContacts");

            return modelAndView;
        }
    }

    /**
     * Method that supports request mapping to allContacts
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to allcontacts.jsp page
     */
    @RequestMapping(value = {"allContacts"})
    public ModelAndView allContacts(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("allcontacts");
        List<MyContact> myContactList = contactService.readAllContacts(principal.getName());

        modelAndView.addObject("allMyContactList", myContactList);
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    /**
     * Method that supports request mapping to deleteContact
     *
     * @param request HttpServletRequest object
     * @return String redirecting to allcontacts.jsp page
     */
    @RequestMapping(value = {"deleteContact"}, method = RequestMethod.POST)
    public String deleteContact(HttpServletRequest request) {

        contactService.deleteContact(Integer.parseInt(request.getParameter("idContact")));

        //tutaj zwracamy redirect zeby nie powtarzac zapytania do db ktore juz jest w allContacts
        return "redirect:/allContacts";
    }
}
