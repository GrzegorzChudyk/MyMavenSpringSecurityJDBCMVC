package com.grzegorz.chudyk.controller;

import com.grzegorz.chudyk.service.AuthoritiesService;
import com.grzegorz.chudyk.utils.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.Arrays;

/**
 * Controller class for Authorities
 *
 * @author Grzegorz Chudyk
 */
@Controller
public class AuthoritiesController {

    @Autowired
    private AuthoritiesService authoritiesService;

    @Autowired
    LoggedUser loggedUser;

    /**
     * Method that supports request mapping to editRoleForm
     *
     * @param httpServletRequest HttpServletRequest object
     * @return ModelAndView to editrole.jsp page
     */
    @RequestMapping(value = {"editRoleForm"}, method = RequestMethod.POST)
    public ModelAndView editRole(HttpServletRequest httpServletRequest, Principal principal) {

        boolean roles[] = authoritiesService.checkUserAuthorities(httpServletRequest.getParameter("changedUserName"));

        ModelAndView modelAndView = new ModelAndView("editrole");
        modelAndView.addObject("changedUserName", httpServletRequest.getParameter("changedUserName"));
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));
        modelAndView.addObject("rolesTable", roles);

        return modelAndView;
    }

    /**
     * Method that supports request mapping to editRoleForm
     *
     * @param httpServletRequest HttpServletRequest object
     * @return String redirecting to allusers.jsp page
     */
    @RequestMapping(value = {"saveRole"}, method = RequestMethod.POST)
    public String saveRole(HttpServletRequest httpServletRequest) {

        //System.out.println("Admin Role: " + httpServletRequest.getParameter("adminRole"));
        //System.out.println("User Role: " + httpServletRequest.getParameter("userRole"));
        String role[] = httpServletRequest.getParameterValues("role");
        if(role == null)
            System.out.println("role jest nullem bleeee");

        //this for roles table
        authoritiesService.editRoles(httpServletRequest.getParameterValues("role"), httpServletRequest.getParameter("changedUserName"));

        //this for each individual role
//        authoritiesService.editRoles(httpServletRequest.getParameter("adminRole"),
//                httpServletRequest.getParameter("userRole"), httpServletRequest.getParameter("changedUserName"));

        return "redirect:/allUsers";
    }
}
