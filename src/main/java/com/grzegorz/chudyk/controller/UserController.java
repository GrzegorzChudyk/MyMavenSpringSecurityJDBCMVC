package com.grzegorz.chudyk.controller;


import com.grzegorz.chudyk.entity.Authorities;
import com.grzegorz.chudyk.entity.MyUser;
import com.grzegorz.chudyk.service.UserService;
import com.grzegorz.chudyk.utils.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Controller class for User
 *
 * @author Grzegorz Chudyk
 */
@Controller
public class UserController {

    @Autowired
    private LoggedUser loggedUser;

    @Autowired
    private UserService userService;

    /**
     * Method that supports request mapping to /
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to home.jsp page
     */
    @RequestMapping(value = {"/"})
    public ModelAndView home(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("home");
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    /**
     * Method that supports request mapping to addUserForm
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to adduser.jsp page
     */
    @RequestMapping(value = {"addUserForm"})
    public ModelAndView addUser(Principal principal){

        ModelAndView modelAndView = new ModelAndView("adduser");
        modelAndView.addObject("myUser", new MyUser());

        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    //nie musi byc "saveContact(HttpServletRequest request)" bo FORM zwraca obiekt od razu @ModelAttribute("contactobj")
    /**
     * Method that supports request mapping to saveUser
     *
     * @param myUser MyUser object received from adduser.jsp page
     * @param result BindingResult object indicate if validation of object has errors
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to adduser.jsp page or redirect to / page
     */
    @RequestMapping(value = {"saveUser"}, method = RequestMethod.POST)
    public ModelAndView saveUser(@Valid @ModelAttribute("myUser") MyUser myUser, BindingResult result, Principal principal) {//}, Model model) {

        //nie bindowalo z tagiem form poniewaz dla tagu form niezbedne bylo wyslanie obiektu np pustego
        //w tym celu w RequestMapping addUserForm nalezalo w modelu wyslac pusty obiekt user!!
        if (result.hasErrors()) {

            myUser.setPassword("");

            ModelAndView modelAndView = new ModelAndView("adduser");
            modelAndView.addObject("myUser", myUser);// new MyUser());
            modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));
            //modelAndView.addObject(model);

            return modelAndView;
        } else {

            Authorities authorities = new Authorities(myUser.getUsername(), "ROLE_USER");

            userService.saveUser(myUser, authorities);

            //tutaj zwracamy redirect w modelu zeby nie powtarzac zapytania do db ktore juz jest w allUsers
            ModelAndView modelAndView = new ModelAndView("redirect:/");

            return modelAndView;
        }
    }

    /**
     * Method that supports request mapping to allUsers
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to allusers.jsp page
     */
    @RequestMapping(value = {"allUsers"})
    public ModelAndView allUsers(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("allusers");
        List<MyUser> myUserList = userService.readAllUsers();

        modelAndView.addObject("allMyUserList", myUserList);
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    /**
     * Method that supports request mapping to deleteUser
     *
     * @param httpServletRequest HttpServletRequest object
     * @return String redirecting to allusers.jsp page
     */
    @RequestMapping(value = {"deleteUser"}, method = RequestMethod.POST)
    public String deleteUser(HttpServletRequest httpServletRequest) {

        userService.deleteUser(Integer.parseInt(httpServletRequest.getParameter("idUser")));

        //tutaj zwracamy redirect w zeby nie powtarzac zapytania do db ktore juz jest w allUsers
        return "redirect:/allUsers";
    }

    /**
     * Method that supports request mapping to changeUserActivity
     *
     * @param httpServletRequest HttpServletRequest object
     * @return String redirecting to allusers.jsp page
     */
    @RequestMapping(value = {"changeUserActivity"}, method = RequestMethod.POST)
    public String changeUserActivity(HttpServletRequest httpServletRequest) {

        userService.changeUserActivity(Integer.parseInt(httpServletRequest.getParameter("idUser")));

        return "redirect:/allUsers";
    }
}
