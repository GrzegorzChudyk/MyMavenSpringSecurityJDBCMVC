package com.grzegorz.chudyk.controller;

import com.grzegorz.chudyk.utils.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

/**
 * Controller class for Login
 *
 * @author Grzegorz Chudyk
 */
@Controller
public class LoginController {

    @Autowired
    private LoggedUser loggedUser;

    /**
     * Method that supports request mapping to login
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to login.jsp page
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(Principal principal) {

        ModelAndView modelAndView = new ModelAndView("login");
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }

    //this request is required for access denied page and it required GET method because of CustomAccessDeniedHandler.class handle method!!
    /**
     * Method that supports request mapping to accessDenied
     *
     * @param principal Principal object that indicate current logged user
     * @return ModelAndView to accessdenied.jsp page
     */
    @RequestMapping(value = "/accessDenied", method = RequestMethod.GET)
    public ModelAndView accessDenied (Principal principal) {

        ModelAndView modelAndView = new ModelAndView("accessdenied");
        modelAndView.addObject("loggedUser", loggedUser.getLoggedUser(principal));

        return modelAndView;
    }
}
