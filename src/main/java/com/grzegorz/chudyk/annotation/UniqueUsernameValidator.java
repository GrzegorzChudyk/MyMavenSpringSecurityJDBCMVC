package com.grzegorz.chudyk.annotation;

import com.grzegorz.chudyk.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Class validating if the username is unique
 *
 * @author Grzegorz Chudyk
 */
@Component
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    @Autowired
    public UserService userService;

    /**
     * Method initializing UniqueUsername annotation
     * @param constraintAnnotation initialized annotation
     */
    public void initialize(UniqueUsername constraintAnnotation) {
    }

    /**
     * Method that initialize validation checking
     *
     * @param value name of validated username
     * @param context constraint validator context
     * @return boolean value if user is unique
     */
    @Transactional
    public boolean isValid(String value, ConstraintValidatorContext context) {

        //The validation runs twice, the first one in the controller and here UserService Bean is not null,
        //the second time the hibernate validates, and here UserService Bean is null.
        // This is the bypass of the second null (bad way ;))
        if (userService == null) {
            return true;
        }

        return userService.uniqueVerification(value);
    }
}
