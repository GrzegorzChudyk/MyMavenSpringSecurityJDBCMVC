package com.grzegorz.chudyk.annotation;


import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

/**
 * Annotation class, creating own annotation to validate if user is unique
 *
 * @author Grzegorz Chudyk
 */
@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = {UniqueUsernameValidator.class})
public @interface UniqueUsername {

    String message() default "Username need to be unique";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
